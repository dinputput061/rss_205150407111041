package com.example.rss_205150407111041.Model;

public class Model {
    private String title;
    private String des;
    private String category;

    public Model(String title, String des, String category) {
        this.title = title;
        this.des = des;
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public String getDes() {
        return des;
    }

    public String getCategory() {
        return category;
    }
}
